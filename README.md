# language-startup-benchmark

When using a script, the speed the language starts up matters the most. Regardless of how fast it can process data, a language that starts up slower will end up slower for a simple script.

This project aims to run a simple benchmark with hello world for many languages.

## Results on my machine

![histogram](results/language-startup-times.png)

Text report. Note that this is averaged from about a hundred 3 digit floats (0.001).

```json
{
  "gcc": 0.0009096385542168681,
  "nim": 0.0010000000000000007,
  "rust": 0.00110843373493976,
  "go": 0.0011144578313253021,
  "lua": 0.0011566265060240972,
  "vala": 0.0011626506024096394,
  "picolisp (kernel only)": 0.0012108433734939767,
  "haskell": 0.0013674698795180732,
  "perl": 0.0020963855421686764,
  "bash": 0.0032710843373493998,
  "zsh": 0.0034337349397590387,
  "picolisp (with pil)": 0.004337349397590365,
  "sbcl": 0.006162650602409641,
  "gauche scheme": 0.009807228915662627,
  "php": 0.010042168674698773,
  "clisp": 0.012789156626506006,
  "guile scheme": 0.013409638554216851,
  "C# (mono)": 0.02028313253012047,
  "python": 0.024168674698795183,
  "gjs": 0.03694578313253008,
  "java": 0.04782530120481928,
  "emacs lisp": 0.05502409638554219,
  "fish": 0.05568072289156628,
  "ruby": 0.056337349397590344,
  "node": 0.058975903614457814,
  "racket/base": 0.06369879518072291,
  "io": 0.08012650602409639,
  "chez scheme": 0.15257831325301202,
  "go (go run)": 0.16513855421686752,
  "racket": 0.3368855421686748,
  "clojure": 0.8786867469879516
}
```

## Startup time for compiled languages

For compiled languages, I'm measuring the speed the compiled executable runs.

## Running

- Run `language-startup-benchmark` for a few times to time the languages and generate reports.

```bash
for i in $(seq 0 100); do
    language-startup-benchmark _reports/$(date --iso-8601=seconds).json
done
```

- Sometimes some compiles will fail. Until I actually manage to fix it or handle it in `process.rkt`, just remove all reports with failures.

```bash
rm "$(grep failed _reports/* --files-with-match)"
```

- Run `process.rkt` to generate a text report, an averaged json report, and a histogram from the files in the folder.

```bash
racket process.rkt _reports
```

Licensed under the MIT license.
